import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import commonEn from './locales/en/common.json';
import commonRu from './locales/ru/common.json';

export const defaultNS = 'common';

export const resources = {
  en: {
    common: commonEn,
  },
  ru: {
    common: commonRu,
  },
} as const;

i18n
  // .use(I18nextBrowserLanguageDetector)
  .use(initReactI18next)
  .init({
    fallbackLng: 'ru',
    detection: {
      lookupLocalStorage: 'lang',
    },
    resources,
    defaultNS,
  });

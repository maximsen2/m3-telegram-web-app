import format from 'date-fns/format';
import localeRU from 'date-fns/locale/ru';
import localeGB from 'date-fns/locale/en-GB';
import parseISO from 'date-fns/parseISO';
import { ParsedDate } from './types';

export const getLocaleByLocaleName = (localeName: string) => {
  if (localeName === 'en') return localeGB;
  return localeRU;
};

export const formatDate = (timestamp: string, locale: string) => {
  if (locale === 'en') {
    return format(parseISO(timestamp), 'dd MMMM yyyy', { locale: localeGB });
  }

  return (
    format(parseISO(timestamp), 'dd MMMM yyyy', { locale: localeRU }) + ' г.'
  );
};

export const formatDateWithDayOfTheWeek = (
  timestamp: string,
  locale: string
) => {
  if (locale === 'en') {
    return format(parseISO(timestamp), 'iiii, dd MMMM yyyy', {
      locale: localeGB,
    });
  }

  return (
    format(parseISO(timestamp), 'iiii, dd MMMM yyyy', { locale: localeRU }) +
    ' г.'
  );
};
// date - "2023-04-01"
export const parseDate = (date: string): ParsedDate => {
  const d = date.split('-').map((i) => Number(i));
  return {
    year: d[0],
    month: d[1],
    day: d[2],
  };
};

export const formatDateToHM = (val: string) => {
  const date = new Date(val.replace(/-/g, '/'));
  const h = date.getHours();
  const m = date.getMinutes();

  const hours = h > 0 ? String(h).padStart(2, '0') + ':' : '';
  const minutes = String(m).padStart(2, '0');
  return hours + minutes;
};

type Words = string[];
const getNumWordRU = (count: number, words: Words) => {
  const number = Math.abs(count) % 100;
  const num = number % 10;

  if (count > 10 && count < 20) return words[2];
  if (num > 1 && num < 5) return words[1];
  if (num === 1) return words[0];
  return words[2];
};

const getNumWordWithCountRU = (count: number, words: Words) => {
  const word = getNumWordRU(count, words);
  return `${count} ${word}`;
};

export const getNumWordWithCount = (
  count: number,
  words: Words,
  locale: string
) => {
  if (locale === 'en') return `${count} ${count > 1 ? words[1] : words[0]}`;
  return getNumWordWithCountRU(count, words);
};

export const getCurrencySign = (currency: string | any) => {
  if (typeof currency !== 'string') return '';

  if (currency === 'RUB') return '₽';
  if (currency === 'USD') return '$';
  if (currency === 'EUR') return '€';

  return currency;
};

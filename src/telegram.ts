import { TelegramWebApps } from '..';

const Telegram = window.Telegram;

const loadingMainButton = (
  text: string,
  options?: TelegramWebApps.MainButtonParams
) => {
  Telegram.WebApp.MainButton.setParams({
    is_visible: true,
    text,
    color: '#D83B3C',
    ...options,
    // @ts-ignore
  }).showProgress();
};

const successMainButton = (
  text: string,
  options?: TelegramWebApps.MainButtonParams
) => {
  Telegram.WebApp.MainButton.setParams({
    is_visible: true,
    text,
    color: '#D83B3C',
    ...options,
    // @ts-ignore
  }).hideProgress();
};

const hideMainButton = () => {
  // telegram.WebApp.MainButton.disable();
  Telegram.WebApp.MainButton.hide();
};

export const telegram = {
  Telegram,
  mainButton: {
    loading: loadingMainButton,
    success: successMainButton,
    hide: hideMainButton,
  },
};

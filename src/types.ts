export type City = {
  id: number;
  name: string;
  salon_count: number;
};

export type Team = {
  id: number;
  name: string;
  phone: string;
  city_id: number;
  city_name: string;
  gps: string;
  gps_desc: string;
  online_payment: number;
  address: string;
  logo_url: string;
};

export type Master = {
  id: number;
  name: string;
  surname: string;
  thirdname: string;
  logo_url: string;
  rating: number;
  price: number;
  currency?: string | null;
};

export type Event = {
  date_time: string;
  id: number;
  status: number;
};

export type ParsedDate = {
  year: number;
  month: number;
  day: number;
};

export type PaymentType = {
  id: number;
  name: string;
};

export type Appointment = {
  city: City | null;
  team: Team | null;
  date: string | null;
  time: Event | null;
  master: Master | null;
  paymentType: PaymentType | null;
};

export type AppointmentType = {
  city: City;
  event: {
    currency: string;
    date_time: string;
    id: number;
    paid: number;
    payment_type: number;
    price: number;
    status: number;
    payment_url?: string;
  };
  master: {
    id: number;
    name: string;
    surname: string;
    thirdname: string;
  };
  salon: Team;

  payment_type?: PaymentType;
};

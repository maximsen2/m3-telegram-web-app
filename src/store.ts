import { configureStore } from '@reduxjs/toolkit';
import { selectionsReducer } from './slices/selectionsSlice';
import { appointmentsReducer } from './slices/appointmentsSlice';

export const store = configureStore({
  reducer: {
    selections: selectionsReducer,
    appointments: appointmentsReducer,
  },
  devTools: false,
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;

import { useDispatch } from 'react-redux';
import { PaymentType } from '../types';
import {
  SelectionsState,
  initialPaymentType,
  setPaymentType,
  setSelections,
  setTime,
} from '../slices/selectionsSlice';
import { City, Event, Master, Team } from '../types';

export const useSelections = () => {
  const dispatch = useDispatch();
  const onCityChange = (city: City) => {
    dispatch(
      setSelections({
        city,
        date: null,
        master: null,
        team: null,
        time: null,
        paymentType: initialPaymentType,
      })
    );
  };
  const onTeamChange = (team: Team) => {
    dispatch(
      setSelections({
        team,
        date: null,
        master: null,
        time: null,
        paymentType: initialPaymentType,
      })
    );
  };
  const onDateChange = (date: string) => {
    dispatch(
      setSelections({
        date,
        time: null,
      })
    );
  };
  const onTimeChange = (event: Event) => {
    dispatch(setTime(event));
  };
  const onMasterChange = (master: Master) => {
    dispatch(
      setSelections({
        master,
        date: null,
      })
    );
  };
  const onPaymentTypeChange = (paymentType: PaymentType) => {
    dispatch(setPaymentType(paymentType));
  };

  const updateSelections = (selections: Partial<SelectionsState>) => {
    dispatch(setSelections(selections));
  };

  const onPromoCodeChange = (promoCode: string) => {
    dispatch(
      setSelections({
        promoCode,
      })
    );
  }

  return {
    onCityChange,
    onTeamChange,
    onDateChange,
    onTimeChange,
    onMasterChange,
    onPaymentTypeChange,
    updateSelections,
    onPromoCodeChange,
  };
};

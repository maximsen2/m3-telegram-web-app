import { useLocation, useNavigate } from 'react-router-dom';

export const usePushWithState = () => {
  const location = useLocation();
  const navigate = useNavigate();

  const push = (to: string, state: { [key in string]: any }) => {
    const st = location.state || {};

    navigate(to, {
      state: { ...st, from: location.pathname, ...state },
    });
  };

  const pushAndClear = (to: string) => {
    navigate(to, { state: {} });
  };

  return {
    state: location.state || {},
    push,
    pushAndClear,
  };
};

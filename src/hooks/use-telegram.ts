import { useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import { selectSelections } from '../slices/selectionsSlice';
import { telegram } from '../telegram';
import { useAppSelector } from './use-redux';
import { Event, PaymentType } from '../types';
import { useSelections } from './use-selections';
import { useMutation } from 'react-query';
import { makeAppointment } from '../api/appointment';
import { selectAppointments } from '../slices/appointmentsSlice';
import { useTranslation } from 'react-i18next';

const useBackButtonHandler = () => {
  const location = useLocation();

  const { data: appointments } = useAppSelector(selectAppointments);

  useEffect(() => {
    console.log('location.pathname', location.pathname);
    if (
      (location.pathname === '/' && !appointments.length) ||
      location.pathname === '/appointments'
    ) {
      telegram.Telegram.WebApp.BackButton.hide();
    } else {
      const fn = () => {
        console.log('backClicked from', location.pathname);
        window.history.back();
      };
      telegram.Telegram.WebApp.BackButton.onClick(fn);
      telegram.Telegram.WebApp.BackButton.show();
      return () => {
        telegram.Telegram.WebApp.BackButton.offClick(fn);
      };
    }
  }, [location.pathname, appointments]);
};

const useMakeAppointmentMutation = ({
  paymentType,
}: {
  paymentType?: PaymentType | null;
}) => {
  const { t } = useTranslation('common');
  const { updateSelections } = useSelections();
  const { mutate } = useMutation(
    ['team-page-query'],
    ({
      eventId,
      paymentTypeId,
      promoCode,
    }: {
      eventId: Event['id'];
      paymentTypeId: PaymentType['id'];
      promoCode?: string;
    }) => {
      telegram.mainButton.loading(t('makeAppointment'));
      return makeAppointment({
        eventId,
        paymentType: paymentTypeId,
        promoCode,
      });
    },
    {
      onError: (err) => {
        console.log(err);
      },
      onSuccess: (data) => {
        if (paymentType?.id === 4) {
          telegram.mainButton.hide();
          telegram.Telegram.WebApp.BackButton.hide();
          window.location.replace(data.data.paymentData.playmentUrl);
        } else {
          updateSelections({
            date: null,
            master: null,
            team: null,
            time: null,
          });

          telegram.Telegram.WebApp.close();
        }
      },
    }
  );

  return { mutate };
};

const useTelegramMainButtonHandler = () => {
  const { city, master, paymentType, team, date, time, promoCode } =
    useAppSelector(selectSelections);
  const { mutate } = useMakeAppointmentMutation({ paymentType });
  const { t, i18n } = useTranslation('common');

  useEffect(() => {
    if (city && master && team && date && time) {
      const fn = () => {
        mutate({ eventId: time.id, paymentTypeId: paymentType.id, promoCode: promoCode ?? undefined });
      };

      telegram.Telegram.WebApp.MainButton.onClick(fn);
      telegram.mainButton.success(t('makeAppointment'));

      return () => {
        telegram.Telegram.WebApp.MainButton.offClick(fn);
      };
    } else {
      telegram.mainButton.hide();
    }
  }, [city, master, team, date, time, i18n.language, promoCode]);
};

const usePaymentSuccessHandler = () => {
  const location = useLocation();
  useEffect(() => {
    if (location.hash === '#paymentSuccess') {
      telegram.Telegram.WebApp.close();
    }
  }, [location.hash]);
};

export const useTelegramHandlers = () => {
  useBackButtonHandler();
  useTelegramMainButtonHandler();
  usePaymentSuccessHandler();
};

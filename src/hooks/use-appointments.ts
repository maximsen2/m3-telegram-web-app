import { useDispatch } from 'react-redux';
import { AppointmentType, PaymentType } from '../types';
import { setAppointments } from '../slices/appointmentsSlice';

export const useAppointments = () => {
  const dispatch = useDispatch();
  const onAppointmentsChange = (appointments: AppointmentType[]) => {
    dispatch(setAppointments(appointments));
  };

  return {
    onAppointmentsChange,
  };
};

import { createRef } from 'react';
import { Provider } from 'react-redux';
import {
  createBrowserRouter,
  RouterProvider,
  useLocation,
  useOutlet,
} from 'react-router-dom';
import { CSSTransition, SwitchTransition } from 'react-transition-group';
import { HomePage } from './pages/index';
import { store } from './store';
import './App.scss';
import { Container } from './components/container';
import { CityPage } from './pages/city-page';
import { TeamPage } from './pages/team-page';
import { CalendarPage } from './pages/calendar-page';
import { MasterPage } from './pages/master-page';
import { PaymentTypePage } from './pages/payment-type';
import { useTelegramHandlers } from './hooks/use-telegram';
import { QueryClientProvider } from 'react-query';
import { queryClient } from './query-client';
import { TimePage } from './pages/time-page';
import { AppointmentsPage } from './pages/appointments-page';
import { AppContextWrapper } from './components/app-context';
import { MapsPage } from './pages/maps-page';

const routes = [
  {
    path: '/',
    name: 'Home',
    element: <HomePage />,
    nodeRef: createRef<HTMLDivElement>(),
  },
  {
    path: '/city',
    name: 'City',
    element: <CityPage />,
    nodeRef: createRef<HTMLDivElement>(),
  },
  {
    path: '/team',
    name: 'Team',
    element: <TeamPage />,
    nodeRef: createRef<HTMLDivElement>(),
  },
  {
    path: '/date',
    name: 'Date',
    element: <CalendarPage />,
    nodeRef: createRef<HTMLDivElement>(),
  },
  {
    path: '/master',
    name: 'Master',
    element: <MasterPage />,
    nodeRef: createRef<HTMLDivElement>(),
  },
  {
    path: '/payment-type',
    name: 'Payment Type',
    element: <PaymentTypePage />,
    nodeRef: createRef<HTMLDivElement>(),
  },
  {
    path: '/time',
    name: 'Time page',
    element: <TimePage />,
    nodeRef: createRef<HTMLDivElement>(),
  },
  {
    path: '/appointments',
    name: 'Appointments',
    element: <AppointmentsPage />,
    nodeRef: createRef<HTMLDivElement>(),
  },
  {
    path: '/maps',
    name: 'Maps',
    element: <MapsPage />,
    nodeRef: createRef<HTMLDivElement>(),
  },
];

const Layout = () => {
  const location = useLocation();
  const currentOutlet = useOutlet();
  useTelegramHandlers();

  const { nodeRef } =
    routes.find((route) => route.path === location.pathname) ?? {};

  return (
    <div className='page_index'>
      <AppContextWrapper>
        <SwitchTransition>
          <CSSTransition
            key={location.pathname}
            nodeRef={nodeRef}
            timeout={300}
            classNames='page'
            unmountOnExit
          >
            {(_state) => (
              <div ref={nodeRef} className='page'>
                <Container>{currentOutlet}</Container>
              </div>
            )}
          </CSSTransition>
        </SwitchTransition>
      </AppContextWrapper>
    </div>
  );
};

function App() {
  return (
    <Provider store={store}>
      <QueryClientProvider client={queryClient}>
        <Layout />
      </QueryClientProvider>
    </Provider>
  );
}

const router = createBrowserRouter(
  [
    {
      path: '/',
      element: <App />,
      children: routes.map((route) => ({
        index: route.path === '/',
        path: route.path === '/' ? undefined : route.path,
        element: route.element,
      })),
    },
  ],
  { basename: '/telegram-app/' }
);

// eslint-disable-next-line import/no-anonymous-default-export
export default () => {
  return <RouterProvider router={router} />;
};

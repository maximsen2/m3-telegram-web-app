import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../store';
import { City, Event, Master, PaymentType, Team } from '../types';

export type SelectionsState = {
  city: City | null;
  team: Team | null;
  date: string | null;
  time: Event | null;
  master: Master | null;
  paymentType: PaymentType;
  promoCode: string | null;
};

export const initialPaymentType: PaymentType = {
  id: 1,
  name: 'Cash',
};


const initialState: SelectionsState = {
  city: null,
  team: null,
  date: null,
  time: null,
  master: null,
  paymentType: initialPaymentType,
  promoCode: null,
};

export const selectionsSlice = createSlice({
  name: 'selections',
  initialState,
  reducers: {
    setCity: (
      state: SelectionsState,
      action: PayloadAction<SelectionsState['city']>
    ) => {
      state = { ...state, city: action.payload };
      return state;
    },
    setTeam: (
      state: SelectionsState,
      action: PayloadAction<SelectionsState['team']>
    ) => {
      state = { ...state, team: action.payload };
      return state;
    },
    setDate: (
      state: SelectionsState,
      action: PayloadAction<SelectionsState['date']>
    ) => {
      state = { ...state, date: action.payload };
      return state;
    },
    setTime: (
      state: SelectionsState,
      action: PayloadAction<SelectionsState['time']>
    ) => {
      state = { ...state, time: action.payload };
      return state;
    },
    setMaster: (
      state: SelectionsState,
      action: PayloadAction<SelectionsState['master']>
    ) => {
      state = { ...state, master: action.payload };
      return state;
    },
    setPaymentType: (
      state: SelectionsState,
      action: PayloadAction<SelectionsState['paymentType']>
    ) => {
      state = { ...state, paymentType: action.payload };
      return state;
    },
    setPromoCode: (state: SelectionsState, action: PayloadAction<SelectionsState['promoCode']> ) => {
      state = { ...state, promoCode: action.payload };
      return state;
    },

    setSelections: (
      state: SelectionsState,
      action: PayloadAction<Partial<SelectionsState>>
    ) => {
      state = {
        ...state,
        ...action.payload,
      };

      return state;
    },
    resetSelections: (state: SelectionsState) => {
      state = initialState;
    },
  },
});

export const {
  setCity,
  setTeam,
  setDate,
  setTime,
  setMaster,
  setPaymentType,
  setSelections,
  resetSelections,
} = selectionsSlice.actions;

export const selectSelections = (state: RootState) => state.selections;

export const selectionsReducer = selectionsSlice.reducer;

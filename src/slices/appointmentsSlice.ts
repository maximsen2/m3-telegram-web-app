import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../store';
import { AppointmentType } from '../types';

export type AppointmentsState = {
  data: AppointmentType[];
};

const initialState: AppointmentsState = {
  data: [],
};

export const appointmentsSlice = createSlice({
  name: 'appointments',
  initialState,
  reducers: {
    setAppointments: (
      state: AppointmentsState,
      action: PayloadAction<AppointmentsState['data']>
    ) => {
      state = { data: action.payload };
      return state;
    },
  },
});

export const { setAppointments } = appointmentsSlice.actions;

export const selectAppointments = (state: RootState) => state.appointments;

export const appointmentsReducer = appointmentsSlice.reducer;

import axios from 'axios';
import { telegram } from '../telegram';

export const baseApi = axios.create({
  baseURL: `/api`,
});

export const baseApiAppointment = axios.create({
  baseURL: `/`,
  params: {
    checkDataString: telegram.Telegram.WebApp.initData,
  },
});

export const updateBaseApiLanguage = (language: string) => {
  [baseApi, baseApiAppointment].forEach((api) =>
    api.interceptors.request.use((config) => {
      config.headers['Accept-Language'] = language;
      return config;
    })
  );
};

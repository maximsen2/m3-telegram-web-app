import { Event, ParsedDate } from '../types';
import { baseApi } from './base-api';

export const fetchMasterDatesByMasterId = async (
  masterId: number,
  selectedDate: { year: number; month: number }
) => {
  const res = await baseApi.get<{ data: { days: string[] | null } }>(
    `/getEventDayListMasterByDate/?master_id=${masterId}&year=${selectedDate.year}&mounth=${selectedDate.month}`
  );
  return res.data.data.days || [];
};

export const fetchMasterDatesBySalonId = async (
  salonId: number,
  selectedDate: { year: number; month: number }
) => {
  const res = await baseApi.get<{ data: { days: string[] | null } }>(
    `/getEventDayListSalonByDate/?salon_id=${salonId}&year=${selectedDate.year}&mounth=${selectedDate.month}`
  );
  return res.data.data.days || [];
};

const getFreeMonthsFromNow = () => {
  const now = new Date();
  const months = [];
  for (let i = 0; i < 3; i++) {
    const date = new Date(now.getFullYear(), now.getMonth() + i);
    months.push(date);
  }
  return months.map((i) => ({
    year: i.getFullYear(),
    month: i.getMonth(),
  }));
}

export const fetchMasterDatesByMasterIdForThreeMonths = async (
  masterId: number
) => {
  const arr = await Promise.all(
    getFreeMonthsFromNow().map(async (i) => {
      const res = await baseApi.get<{ data: { days: string[] | null } }>(
        `/getEventDayListMasterByDate/?master_id=${masterId}&year=${i.year}&mounth=${i.month}`
      );
      return res.data.data.days || [];
    })
  );

  return arr.flat();
};

export const fetchMasterDatesBySalonIdForThreeMonths = async (
  salonId: number
) => {
  const arr = await Promise.all(
    getFreeMonthsFromNow().map(async (i) => {
      const res = await baseApi.get<{ data: { days: string[] | null } }>(
        `/getEventDayListSalonByDate/?salon_id=${salonId}&year=${i.year}&mounth=${i.month}`
      );

      return res.data.data.days || [];
    })
  );

  return arr.flat();
};
//test.m3world.ru/api/getEventListMasterByDate/
// https:
export const fetchMasterTimeByMasterId = async (
  masterId: number,
  date: ParsedDate
) => {
  const res = await baseApi.get<{ data: { events: Event[] | null } }>(
    `/getEventListMasterByDate/?master_id=${masterId}&year=${date.year}&mounth=${date.month}&day=${date.day}`
  );

  return res.data.data.events || [];
};

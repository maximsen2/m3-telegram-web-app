import { Master } from '../types';
import { baseApi } from './base-api';

const transformMaster = (master: Master): Master => {
  if (!master.logo_url) return master;
  return {
    ...master,
    logo_url: master.logo_url.replace('http:', 'https:'),
  };
};

export const fetchMastersByTeamId = async (teamId: number) => {
  const res = await baseApi.get<{ data: { masters: Master[] } }>(
    `/getMasterListBySalon/?salon_id=${teamId}`
  );
  return res.data.data.masters.map(transformMaster);
};

export const fetchMasterListBySalonIdAndDate = async (
  salonId: number,
  selectedDate: { year: number; month: number; day: number }
) => {
  const res = await baseApi.get<{ data: { masters: Master[] } }>(
    `/getMasterListSalonByDate/?salon_id=${salonId}&year=${selectedDate.year}&mounth=${selectedDate.month}&day=${selectedDate.day}`
  );
  return res.data.data.masters.map(transformMaster);
};

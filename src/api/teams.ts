import { Team } from '../types';
import { baseApi } from './base-api';

export const fetchTeamsByCityId = async (cityId: number) => {
  const res = await baseApi.get<{ data: { salons: Team[] } }>(
    `/getSalonList/?city_id=${cityId}`
  );
  return res.data.data.salons.map((salon) => ({
    ...salon,
    logo_url: salon.logo_url && salon.logo_url.replace('http:', 'https:'),
  }));
};

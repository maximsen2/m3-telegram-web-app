import { AppointmentType, PaymentType } from '../types';
import { Event } from '../types';
import { baseApiAppointment } from './base-api';
import { fetchTeamsByCityId } from './teams';

export const fetchAppointments = async () => {
  const res = await baseApiAppointment.get<{
    data: { events: AppointmentType[] };
  }>('/customerBot/getRecordEvents');

  const updatedEvents = Promise.all(
    res.data.data.events.map(async (events) => {
      const salonsOfTheCity = await fetchTeamsByCityId(events.city.id);
      const salon = salonsOfTheCity.find((i) => i.id === events.salon.id);
      return {
        ...events,
        salon: {
          ...events.salon,
          ...salon,
        },
      };
    })
  );
  return updatedEvents;
};

type MakeAppointmentResponse = {
  data: {
    paymentData: {
      requestResult: {
        Success: boolean;
        ErrorCode: string;
        TerminalKey: string;
        Status: string;
        PaymentId: string;
        OrderId: string;
        Amount: number;
        PaymentURL: string;
      };
      playmentUrl: string;
    };
  };
};

export const makeAppointment = async (params: {
  eventId: Event['id'];
  paymentType: PaymentType['id'];
  promoCode?: string;
}) => {
  const res = await baseApiAppointment.get<MakeAppointmentResponse>(
    '/customerBot/recordEvent',
    {
      params,
    }
  );

  return res.data;
};

export const cancelAppointment = async (params: { eventId: Event['id'] }) => {
  const res = await baseApiAppointment.get<{}>(
    '/customerBot/cancelRecordEvent',
    {
      params,
    }
  );
  return res.data;
};

export const fetchPaymentTypes = async (params: { salonId: number }) => {
  const res = await baseApiAppointment.get<{
    data: { payment_types: PaymentType[] };
  }>('/customerBot/getPaymentTypes', {
    params,
  });

  return res.data.data.payment_types;
};

export const fetchLanguage = async () => {
  const res = await baseApiAppointment.get<{ data: { language: string } }>(
    '/customerBot/getUserLanguage'
  );
  return res.data.data.language;
};

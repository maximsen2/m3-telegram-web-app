import { City } from '../types';
import { baseApi } from './base-api';

export const fetchCities = async () => {
  const res = await baseApi.get<{ data: { cities: City[] } }>(
    '/getCityListWithSalon'
  );
  return res.data.data.cities;
};

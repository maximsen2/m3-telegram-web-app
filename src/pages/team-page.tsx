import { useQuery } from 'react-query';
import { useNavigate } from 'react-router-dom';
import { fetchTeamsByCityId } from '../api/teams';
import { Button } from '../components/button';
import { useAppSelector } from '../hooks/use-redux';
import { useSelections } from '../hooks/use-selections';
import { selectSelections } from '../slices/selectionsSlice';
import { Team } from '../types';
import { Loader } from '../components/loader';
import { SquareDiv } from '../components/squared-div';

export const TeamPage = () => {
  const { onTeamChange } = useSelections();
  const { city } = useAppSelector(selectSelections);
  const navigate = useNavigate();

  const { data, isFetching } = useQuery(
    ['team-page-query', city?.id],
    () => fetchTeamsByCityId(city?.id || 0),
    {
      enabled: typeof city?.id === 'number',
      initialData: [],
    }
  );

  const onChange = (team: Team) => {
    onTeamChange(team);
    navigate(-1);
  };

  if (isFetching) {
    return <Loader />;
  }

  return (
    <div>
      {data?.map((i) => (
        <Button
          key={i.id}
          icon={
            i.logo_url ? (
              <SquareDiv>
                <img
                  loading='lazy'
                  className='full-height'
                  alt={i.name}
                  src={i.logo_url}
                />
              </SquareDiv>
            ) : undefined
          }
          onClick={() => onChange(i)}
        >
          {i.name}
        </Button>
      ))}
    </div>
  );
};

import { useQuery } from 'react-query';
import { useNavigate } from 'react-router-dom';
import { fetchCities } from '../api/cities';
import { Button } from '../components/button';
import { useSelections } from '../hooks/use-selections';
import { City } from '../types';
import { getNumWordWithCount } from '../utils';
import { Loader } from '../components/loader';
import { useTranslation } from 'react-i18next';

export const CityPage = () => {
  const { onCityChange } = useSelections();
  const { i18n } = useTranslation();
  const navigate = useNavigate();
  const onChange = (city: City) => {
    onCityChange(city);
    navigate(-1);
  };

  const { data, isFetching } = useQuery(['city-page-query'], fetchCities, {
    initialData: [],
  });

  if (isFetching) {
    return <Loader />;
  }

  return (
    <>
      {data?.map((i) => (
        <Button
          key={i.id}
          onClick={() => onChange(i)}
          textRight={getNumWordWithCount(
            i.salon_count,
            i18n.language === 'en'
              ? ['team', 'teams']
              : ['команда', 'команды', 'команд'],
            i18n.language
          )}
        >
          {i.name}
        </Button>
      ))}
    </>
  );
};

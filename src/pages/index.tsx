import {
  AddressIcon,
  CalendarIcon,
  HumanIcon,
  LocationIcon,
} from '../components/icons';
import { NavLink } from '../components/nav-link';
import { SquareDiv } from '../components/squared-div';
import { Title } from '../components/title';
import { useAppSelector } from '../hooks/use-redux';
import { selectSelections } from '../slices/selectionsSlice';
import { Master } from '../types';
import { formatDate, formatDateToHM } from '../utils';
import { useTranslation } from 'react-i18next';
import { ChangeEvent, useCallback, useMemo } from 'react';
import { Input } from '../components/input';
import { useSelections } from '../hooks/use-selections';

// const FakeTelegramButtonForDev = ({
//   text,
//   onClick,
// }: {
//   text: string;
//   onClick: () => void;
// }) => {
//   return (
//     <button
//       style={{
//         position: 'fixed',
//         bottom: 0,
//         left: 0,
//         right: 0,
//         padding: 20,
//         textAlign: 'center',
//         backgroundColor: 'red',
//         color: 'white',
//       }}
//       onClick={onClick}
//     >
//       {text}
//     </button>
//   );
// };

const MasterImage = ({ master }: { master: Master | null }) => {
  if (master && master.logo_url) {
    return <img src={master.logo_url} alt={master.name} />;
  }

  return <HumanIcon width={23} height={23} />;
};

export const HomePage = () => {
  const { t, i18n } = useTranslation('common');
  const { city, team, date, master, time, promoCode } =
    useAppSelector(selectSelections);

  const { onPromoCodeChange } = useSelections();

  const formattedDate = useMemo(
    () => (date ? formatDate(date, i18n.language) : t('home.selectDate')),
    [date, i18n.language, t]
  );
  const formattedTime = useMemo(
    () => (time ? ` ${formatDateToHM(time.date_time)}` : null),
    [time]
  );

  const handlePromoCodeChange = useCallback((e: ChangeEvent<HTMLInputElement>) => {
    onPromoCodeChange(e.target.value);
  }, [onPromoCodeChange])

  return (
    <>
      <Title>{t('home.where')}</Title>
      <NavLink
        to='/city'
        active={city !== null}
        icon={<LocationIcon width={23} height={23} />}
      >
        {city?.name || t('home.selectCity')}
      </NavLink>
      {city && (
        <NavLink
          to='/team'
          active={team !== null}
          icon={
            team?.logo_url ? (
              <SquareDiv>
                <img
                  loading='lazy'
                  className='full-height'
                  alt={team.name}
                  src={team.logo_url}
                />
              </SquareDiv>
            ) : (
              <AddressIcon width={23} height={23} />
            )
          }
        >
          {team?.name || t('home.selectTeam')}
        </NavLink>
      )}
      {city && team && (
        <>
          <Title>{t('home.when')}</Title>
          <NavLink
            to='/date'
            active={date !== null}
            icon={<CalendarIcon width={23} height={23} />}
          >
            {formattedDate}
            {formattedTime}
          </NavLink>
          <Title>{t('master.master')}</Title>
          <NavLink
            to='/master'
            active={master !== null}
            icon={<MasterImage master={master} />}
          >
            {master?.name || t('home.selectMaster')}
          </NavLink>
          {/* <Title>{t('home.paymentMethod')}</Title>
          <NavLink
            to='/payment-type'
            active={paymentType !== null}
            icon={<PaymentIconView paymentType={paymentType} />}
          >
            {paymentType?.name || t('home.selectPaymentMethod')}
          </NavLink> */}
          <Title>{t('home.promoCode')}</Title>
          <Input placeholder={t('home.enterPromoCode')} value={promoCode ?? undefined} onChange={handlePromoCodeChange} />
        </>
      )}

      {/* {city && master && team && date && time && (
        <FakeTelegramButtonForDev
          text='Записаться'
          onClick={() =>
            mutate({ eventId: time.id, paymentTypeId: paymentType.id, promoCode: promoCode ?? undefined})
          }
        />
      )} */}
    </>
  );
};

import { useEffect, useState } from 'react';
import { useQuery } from 'react-query';
import { useNavigate } from 'react-router-dom';
import { fetchMasterTimeByMasterId } from '../api/master-dates';
import { Profile } from '../components/profile';
import { TimeList } from '../components/time-list';
import { usePushWithState } from '../hooks/use-push-with-state';
import { useSelections } from '../hooks/use-selections';
import { Event } from '../types';
import { parseDate } from '../utils';
import { Loader } from '../components/loader';
import { useAppSelector } from '../hooks/use-redux';
import { selectSelections } from '../slices/selectionsSlice';

export const TimePage = () => {
  const { state, push } = usePushWithState();
  const { updateSelections } = useSelections();
  const [master] = useState(state.master);
  const navigate = useNavigate();
  const { paymentType } = useAppSelector(selectSelections);

  useEffect(() => {
    if (!master || !state.date) {
      navigate('/');
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const { data, isFetching } = useQuery(
    ['master-time-page-query', master?.id],
    () =>
      fetchMasterTimeByMasterId(master?.id || 0, parseDate(state.date || '')),
    {
      enabled: typeof master?.id === 'number' && typeof state.date === 'string',
      initialData: [],
      cacheTime: 0,
      refetchOnMount: true,
    }
  );

  if (!master || !data) return null;

  const onChange = (time: Event) => {
    const master = state.master;
    const date = state.date;
    if (master && date) {
      updateSelections({ master, time, date });
      push('/', { }) 
      // if (paymentType) {
      //   push('/', { }) 
      // } else {
      //   push('/payment-type', {});
      // }
    }
  };
  return (
    <div>
      <Profile master={master} />
      {isFetching ? <Loader /> : <TimeList events={data} onClick={onChange} />}
    </div>
  );
};

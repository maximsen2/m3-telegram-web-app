import { ru as locale } from 'date-fns/locale';
//@ts-ignore
import buildLocalizeFn from 'date-fns/locale/_lib/buildLocalizeFn';

// Для календаря, тк тот берет дни недели из days.abbreviated, для русского это 'вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'суб', что не совсем привычно
const dayValues = {
  narrow: ['В', 'П', 'В', 'С', 'Ч', 'П', 'С'],
  short: ['вс', 'пн', 'вт', 'ср', 'чт', 'пт', 'сб'],
  abbreviated: ['вс', 'пн', 'вт', 'ср', 'чт', 'пт', 'сб'],
  wide: [
    'воскресенье',
    'понедельник',
    'вторник',
    'среда',
    'четверг',
    'пятница',
    'суббота',
  ],
};

locale.localize!.day = buildLocalizeFn({
  values: dayValues,
  defaultWidth: 'wide',
  defaultFormattingWidth: 'wide',
});

import { useEffect, useMemo } from 'react';
import { useQuery } from 'react-query';
import {
  fetchMasterDatesByMasterIdForThreeMonths,
  fetchMasterDatesBySalonIdForThreeMonths,
} from '../api/master-dates';
import { usePushWithState } from '../hooks/use-push-with-state';
import { useAppSelector } from '../hooks/use-redux';
import { selectSelections } from '../slices/selectionsSlice';
import 'react-nice-dates/build/style.css';
import { Calendar } from 'react-nice-dates';
import format from 'date-fns/format';
import { Loader } from '../components/loader';
import { getLocaleByLocaleName } from '../utils';
import { useTranslation } from 'react-i18next';
import styles from './calendar-page.module.scss';
import './locale';

const mapDates = (dates: string[] | undefined) => {
  const result: { [key in number]: boolean } = {};

  if (dates) {
    dates.forEach((d) => {
      result[new Date(d).setHours(0, 0, 0, 0)] = true;
    });
  }
  return result;
};

export const CalendarPage = () => {
  const { state, push, pushAndClear } = usePushWithState();
  const { team } = useAppSelector(selectSelections);
  const { i18n } = useTranslation();
  const locale = useMemo(
    () => getLocaleByLocaleName(i18n.language),
    [i18n.language]
  );

  const dataFromState = useMemo(
    () =>
      state.from === '/master' && state.master
        ? { from: 'master', master: state.master }
        : state.from === '/'
        ? { type: 'team' }
        : null,
    [state]
  );

  useEffect(() => {
    if (!dataFromState) {
      pushAndClear('/');
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const onChange = (date: string) => {
    if (state.from === '/master') {
      push('/time', { date });
    } else {
      push('/master', { date });
    }
  };
  const { data, isFetching } = useQuery(
    ['calendar-page-query', dataFromState?.type, team?.id],
    () =>
      dataFromState?.from === 'master'
        ? fetchMasterDatesByMasterIdForThreeMonths(
            dataFromState.master.id as any
          )
        : fetchMasterDatesBySalonIdForThreeMonths(team?.id || 0),
    {
      enabled: dataFromState && team ? true : false,
      initialData: [],
      keepPreviousData: true,
      cacheTime: 500,
    }
  );

  if (isFetching && !data?.length) {
    return <Loader />;
  }

  const mapped = mapDates(data);
  const handleDayClick = (date: Date | null) => {
    if (date) {
      onChange(format(date, 'yyyy-MM-dd'));
    }
  };

  return (
    <div className={styles.wrapper}>
      <Calendar
        onDayClick={handleDayClick}
        modifiers={{
          highlight: (date) => {
            return mapped[date.getTime()] || false;
          },
          disabled: (date) => {
            return !mapped[date.getTime()];
          },
        }}
        modifiersClassNames={{
          highlight: '-highlight',
        }}
        locale={locale}
      />
    </div>
  );
};

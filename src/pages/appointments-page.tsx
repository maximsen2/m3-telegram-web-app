import { useNavigate } from 'react-router-dom';
import { Title } from '../components/title';
import { useAppSelector } from '../hooks/use-redux';
import { selectAppointments } from '../slices/appointmentsSlice';
import { useEffect } from 'react';
import { MainButton } from '../components/main-button';
import { AppointmentCardList } from '../components/appointment-card-list';
import styles from './appointments-page.module.scss';
import { useTranslation } from 'react-i18next';

export const AppointmentsPage = () => {
  const { data } = useAppSelector(selectAppointments);
  const navigate = useNavigate();
  console.log('data', data);
  useEffect(() => {
    if (!data.length) {
      navigate('/');
    }
  }, [data]);

  console.log(data);

  const { t } = useTranslation('common');
  return (
    <div>
      <div className={styles.mainButtonWrapper}>
        <MainButton isLink to='/' text={t('appointments.makeNewAppointment')} />
      </div>

      <Title>{t('appointments.appointments')}</Title>

      <AppointmentCardList data={data} />
    </div>
  );
};

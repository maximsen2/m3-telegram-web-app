import { useEffect } from 'react';
import { useQuery } from 'react-query';
import { useNavigate } from 'react-router-dom';
import {
  fetchMasterListBySalonIdAndDate,
  fetchMastersByTeamId,
} from '../api/masters';
import { Button } from '../components/button';
import { SquareDiv } from '../components/squared-div';
import { usePushWithState } from '../hooks/use-push-with-state';
import { useAppSelector } from '../hooks/use-redux';
import { selectSelections } from '../slices/selectionsSlice';
import { Master } from '../types';
import { getCurrencySign, parseDate } from '../utils';
import { Loader } from '../components/loader';
import styles from './master-page.module.scss';

export const MasterPage = () => {
  const { team } = useAppSelector(selectSelections);
  const navigate = useNavigate();
  const { push, state } = usePushWithState();

  const { data, isFetching } = useQuery(
    ['master-page-query', team?.id, state.date],
    () =>
      state.from === '/date'
        ? fetchMasterListBySalonIdAndDate(team?.id || 0, parseDate(state.date))
        : fetchMastersByTeamId(team?.id || 0),
    {
      enabled: typeof team?.id === 'number',
      initialData: [],
      keepPreviousData: true,
      cacheTime: 500,
    }
  );

  useEffect(() => {
    if (!team) {
      navigate('/');
    }
  }, [navigate, team]);

  const onChange = (master: Master) => {
    if (state.from !== '/date') {
      push('/date', { master });
    } else {
      push('/time', { master });
    }
  };

  if (isFetching && !data?.length) {
    return <Loader />;
  }

  return (
    <div>
      {data?.map((i) => (
        <Button
          key={i.id}
          onClick={() => onChange(i)}
          textRight={
            <span className={styles.buttonBudge}>
              {i.price} {getCurrencySign(i.currency)}
            </span>
          }
          icon={
            <SquareDiv>
              <img
                loading='lazy'
                alt={i.name}
                className='full-height'
                src={i.logo_url}
              />
            </SquareDiv>
          }
        >
          {i.name} {i.surname}
        </Button>
      ))}
    </div>
  );
};

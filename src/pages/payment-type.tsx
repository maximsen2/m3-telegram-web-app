import { Button } from '../components/button';
import { PaymentType } from '../types';
import { useSelections } from '../hooks/use-selections';
import { PaymentIconView } from '../components/payment-icon-view';
import { selectSelections } from '../slices/selectionsSlice';
import { useAppSelector } from '../hooks/use-redux';
import { fetchPaymentTypes } from '../api/appointment';
import { useQuery } from 'react-query';
import { Loader } from '../components/loader';
import { usePushWithState } from '../hooks/use-push-with-state';

export const PaymentTypePage = () => {
  const { onPaymentTypeChange } = useSelections();
  const { team } = useAppSelector(selectSelections);
  const { push } = usePushWithState();

  const { data, isFetching } = useQuery(
    ['team-page-query', team?.id],
    () => fetchPaymentTypes({ salonId: team?.id || 0 }),
    {
      enabled: typeof team?.id === 'number',
      initialData: [],
    }
  );

  if (!data?.length && isFetching) {
    return <Loader />;
  }
  console.log(data);

  const onChange = (paymentType: PaymentType) => {
    onPaymentTypeChange(paymentType);
    push('/', {});
  };

  return (
    <div>
      {data?.map((i) => (
        <Button
          key={i.id}
          icon={<PaymentIconView paymentType={i} isActive />}
          onClick={() => onChange(i)}
        >
          {i.name}
        </Button>
      ))}
    </div>
  );
};

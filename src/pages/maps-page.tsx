import { Maps } from '../components/maps';
import { usePushWithState } from '../hooks/use-push-with-state';

export const MapsPage = () => {
  const { state } = usePushWithState();
  const { props, lat, lng } = state;

  console.log(lat, lng, props);
  if (!lat || !lng) {
    return null;
  }

  return (
    <Maps
      appointment={props}
      cords={{
        lng,
        lat,
      }}
    />
  );
};

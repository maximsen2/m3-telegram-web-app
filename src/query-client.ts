import { QueryClient } from 'react-query';

export const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      refetchOnWindowFocus: false,
      onError: (err) => {
        const r = document.getElementById('root');
        const error = document.createElement('div');
        // @ts-ignore
        error.textContent = err.message;
        if (r) {
          r.appendChild(error);
        }
      },
    },
  },
});

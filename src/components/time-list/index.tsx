import { useTranslation } from 'react-i18next';
import { Event } from '../../types';
import { formatDateToHM } from '../../utils';
import { Placeholders } from './placeholders';
import styles from './time-list.module.scss';

export const TimeList = ({
  events,
  onClick,
}: {
  events: Event[];
  onClick: (t: Event) => void;
}) => {
  const { t } = useTranslation('common');

  return (
    <div className={styles.wrapper}>
      <div className={styles.title}>{t('master.availableTime')}</div>
      <div className={styles.timeList}>
        {events.map((i) => (
          <button
            key={i.id}
            onClick={() => onClick(i)}
            className={styles.timeItem}
          >
            {formatDateToHM(i.date_time)}
          </button>
        ))}
        <Placeholders />
      </div>
    </div>
  );
};

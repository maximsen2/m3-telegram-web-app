import styles from './time-list.module.scss';
const arr = Array.from(Array(10).keys());
export const Placeholders = () => {
  return (
    <>
      {arr.map((idx) => (
        <span key={idx} className={styles.placeholder}></span>
      ))}
    </>
  );
};

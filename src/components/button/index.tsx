import classNames from 'classnames';
import { CSSProperties } from 'react';
import styles from './button.module.scss';

type Props = {
  children: React.ReactNode;
  textRight?: React.ReactNode | string;
  onClick?: () => void;
  icon?: React.ReactNode;
  size?: 'default' | 'small';
  style?: CSSProperties;
};

export const Button = ({
  children,
  textRight,
  onClick,
  icon,
  size,
  style,
}: Props) => {
  let className = classNames(styles.button, size === 'small' && styles.small);

  return (
    <span onClick={onClick} role='button' className={className} style={style}>
      {icon && <span className={styles.icon}>{icon}</span>}
      <span className={styles.content}>{children}</span>{' '}
      {textRight && <span className={styles.textRight}>{textRight}</span>}
    </span>
  );
};

import { SpinnerIcon } from '../icons';
import styles from './loader.module.scss';

export const Loader = () => {
  return (
    <div className={styles.loader}>
      <SpinnerIcon />
    </div>
  );
};

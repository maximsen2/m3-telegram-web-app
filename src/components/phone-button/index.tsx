import { PhoneIcon } from '../icons';
import styles from './phone-button.module.scss';

export const PhoneButton = ({ phone }: { phone: string }) => {
  return (
    <a className={styles.button} href={`tel:${phone}`}>
      <PhoneIcon />
    </a>
  );
};

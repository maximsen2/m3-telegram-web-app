import { useTranslation } from 'react-i18next';
import { Master } from '../../types';
import styles from './profile.module.scss';
import { SquareDiv } from '../squared-div';
import { getCurrencySign } from '../../utils';

export const Profile = ({ master }: { master: Master }) => {
  const { t } = useTranslation('common');
  return (
    <div className={styles.wrapper}>
      <SquareDiv maxWidth={200} styles={{ borderRadius: '50%' }}>
        <img
          loading='lazy'
          className={styles.image}
          alt={master.name}
          src={master.logo_url}
        />
      </SquareDiv>
      <div className={styles.name}>
        {t('master.master')} {master.name} {master.surname}
      </div>
      <div className={styles.price}>
        {t('master.price', {
          price: master.price,
          currency: getCurrencySign(master.currency),
        })}
      </div>
    </div>
  );
};

import { createContext, useContext } from 'react';
import { useAppointments } from '../../hooks/use-appointments';
import { useAppSelector } from '../../hooks/use-redux';
import { useNavigate } from 'react-router-dom';
import { fetchAppointments, fetchLanguage } from '../../api/appointment';
import { selectAppointments } from '../../slices/appointmentsSlice';
import { useQuery } from 'react-query';
import { Loader } from '../loader';
import { useTranslation } from 'react-i18next';
import { updateBaseApiLanguage } from '../../api/base-api';
import { telegram } from '../../telegram';

export type AppContextType = {
  isFetching: boolean;
  error: any;
  refetch: () => void;
};

export const AppContext = createContext<AppContextType>({
  isFetching: true,
  error: null,
  refetch: () => {},
});

export const useAppContext = () => {
  return useContext(AppContext);
};

const useAppQuery = () => {
  const { onAppointmentsChange } = useAppointments();
  const { data } = useAppSelector(selectAppointments);
  const { i18n } = useTranslation();

  const changeLanguage = (language: string) => {
    const lang = language === 'en' ? 'en' : 'ru';
    i18n.changeLanguage(lang);
  };
  const navigate = useNavigate();
  const { isFetching, error, refetch, isFetchedAfterMount } = useQuery(
    ['app-context-query'],
    async () => {
      const language = await fetchLanguage();
      changeLanguage(language);
      updateBaseApiLanguage(language);

      const appointments = await fetchAppointments();
      onAppointmentsChange(appointments);

      return { language, appointments };
    },
    {
      initialData: { language: 'ru', appointments: [] },
      enabled: !!telegram.Telegram.WebApp.initData && data.length === 0,
      retry: false,
      onError: (err) => {
        console.log(err);
      },

      onSuccess: (data) => {
        if (data.appointments.length) {
          navigate('/appointments');
        }
      },
    }
  );

  return { isFetching: isFetching && !isFetchedAfterMount, error, refetch };
};

export const AppContextWrapper = ({
  children,
}: {
  children: React.ReactNode;
}) => {
  const { isFetching, error, refetch } = useAppQuery();
  if (isFetching) {
    return <Loader />;
  }

  // @ts-ignore
  if (!telegram.Telegram.WebApp.initData || error?.response?.status === 423) {
    return (
      <div>
        Ошибка авторизации, попробуйте пройти регистрацию заново отправив боту
        сообщение ‘/start’
      </div>
    );
  }

  const value: AppContextType = {
    isFetching,
    refetch,
    error,
  };
  return <AppContext.Provider value={value}>{children}</AppContext.Provider>;
};

import classNames from 'classnames';
import { PaymentType } from '../../types';
import { PaymentAlternativeIcon, PaymentIcon } from '../icons';
import styles from './payment-icon-view.module.scss';

type Props = {
  paymentType: PaymentType | null;
  isActive?: boolean;
};

export const PaymentIconView = ({ paymentType, isActive }: Props) => {
  const className = classNames(styles.icon, isActive && styles.active);
  if (paymentType?.id === 1) {
    return (
      <PaymentAlternativeIcon className={className} width={23} height={23} />
    );
  }

  return <PaymentIcon className={className} width={23} height={23} />;
};

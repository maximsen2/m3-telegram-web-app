import { GoogleMap, useJsApiLoader, Marker } from '@react-google-maps/api';
import { AppointmentType } from '../../types';
import styles from './maps.module.scss';
import { Button } from '../button';
import { LocationIcon } from '../icons';

const apiKey = 'AIzaSyDu98qDcoT4oXjyDWrhJrC3uUH8IE-vjAA';

type Props = {
  cords: {
    lat: number;
    lng: number;
  };
  appointment?: AppointmentType;
};

export const Maps = ({ cords, appointment }: Props) => {
  const { isLoaded } = useJsApiLoader({
    googleMapsApiKey: apiKey,
  });

  if (!isLoaded) {
    return null;
  }

  console.log(appointment);

  return (
    <div className={styles.wrapper}>
      <div className={styles.addressButton}>
        <Button icon={<LocationIcon width={23} height={23} />}>
          {appointment?.city.name}, {appointment?.salon.address}
        </Button>
      </div>
      <GoogleMap
        center={cords}
        options={{
          zoomControl: false,
          streetViewControl: false,
          mapTypeControl: false,
          fullscreenControl: false,
        }}
        zoom={15}
        mapContainerStyle={{ width: '100%', height: '100%' }}
      >
        <Marker
          position={cords}
          label={
            appointment?.salon.gps_desc
              ? {
                  text: appointment.salon.gps_desc,
                  className: styles.markerLabel,
                }
              : undefined
          }
          visible
          options={{
            visible: true,
          }}
        />
      </GoogleMap>
      {/* {appointment?.salon.extended && appointment.salon.phone && (
        <PhoneButton phone={appointment.salon.phone} />
      )} */}
    </div>
  );
};

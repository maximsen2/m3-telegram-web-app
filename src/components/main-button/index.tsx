import classNames from 'classnames';
import { CSSProperties } from 'react';
import { Link } from 'react-router-dom';
import styles from './main-button.module.scss';

type Props = {
  isLoading?: boolean;
  text: string;
  style?: CSSProperties;
} & (ButtonProps | LinkProps);

type ButtonProps = {
  onClick: () => void;
  isLoading?: boolean;
  isLink?: false;
};

type LinkProps = {
  to: string;
  isLink: true;
};

export const MainButton = (props: Props) => {
  if (props.isLink) {
    return (
      <Link className={styles.button} to={props.to}>
        {props.text}
      </Link>
    );
  }

  return (
    <button
      className={classNames(styles.button, props.isLoading && styles.disabled)}
      onClick={props.onClick}
      type='button'
      disabled={props.isLoading}
      style={props.style}
    >
      {props.text}
    </button>
  );
};

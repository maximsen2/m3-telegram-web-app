import { Link, useLocation } from 'react-router-dom';
import { ArrowDownIcon } from '../icons';
import styles from './nav-link.module.scss';

type Props = {
  children: React.ReactNode;
  to?: string;
  disableExpandIcon?: boolean;
  icon?: React.ReactNode;
  active?: boolean;
  size?: 'default' | 'small';
};

export const NavLink = ({
  children,
  to,
  disableExpandIcon,
  icon,
  active,
  size = 'default',
}: Props) => {
  const location = useLocation();
  let classes = styles.link;
  if (active) classes += ` ${styles.active}`;
  if (size === 'small') classes += ` ${styles.small}`;

  if (to) {
    return (
      <Link className={classes} to={to} state={{ from: location.pathname }}>
        {icon && <span className={styles.icon}>{icon}</span>}
        {children}{' '}
        {disableExpandIcon || (
          <span className={styles.expandIcon}>
            <ArrowDownIcon width='20' height='20' />
          </span>
        )}
      </Link>
    );
  }

  return (
    <span className={classes}>
      {icon && <span className={styles.icon}>{icon}</span>}
      {children}{' '}
      {disableExpandIcon || (
        <span className={styles.expandIcon}>
          <ArrowDownIcon width='20' height='20' />
        </span>
      )}
    </span>
  );
};

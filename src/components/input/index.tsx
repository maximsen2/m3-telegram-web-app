import classNames from 'classnames';
import { HTMLProps } from 'react';
import styles from './index.module.scss';

type Props = HTMLProps<HTMLInputElement>;

export const Input = ({
  className,
  ...props
}: Props) => {
  const inputClassName = classNames(styles.input, className);

  return (
    <input className={inputClassName} {...props} />
  );
};

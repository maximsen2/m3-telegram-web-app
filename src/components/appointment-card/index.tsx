import { useMutation } from 'react-query';
import { AppointmentType, Event } from '../../types';
import {
  formatDateWithDayOfTheWeek,
  getCurrencySign,
  formatDateToHM,
} from '../../utils';
import { PaymentIconView } from '../payment-icon-view';
import { useAppContext } from '../app-context';
import { Button } from '../button';
import {
  ArrowDownIcon,
  CalendarIcon,
  ClockIcon,
  HumanIcon,
  LocationIcon,
} from '../icons';
import { MainButton } from '../main-button';
import { cancelAppointment } from '../../api/appointment';
import styles from './appointment-card.module.scss';
import { usePushWithState } from '../../hooks/use-push-with-state';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';
import { useMemo } from 'react';

const TotalPrice = ({
  price,
  paymentRequired,
  paymentUrl,
}: {
  price: string;
  paymentRequired?: boolean;
  paymentUrl?: string;
}) => {
  const { t } = useTranslation('common');
  return (
    <span className={styles.totalPrice}>
      {t('appointments.total')}: {price}
      {paymentRequired && (
        <a className={styles.payLink} href={paymentUrl}>
          {t('appointments.pay')}
        </a>
      )}
    </span>
  );
};

const ShowOnMap = ({
  text,
  onClick,
}: {
  text: string;
  onClick: () => void;
}) => {
  return (
    <button onClick={onClick} className={styles.showOnMap}>
      {text} <ArrowDownIcon width={23} height={23} />
    </button>
  );
};

const useCancelAppointmentMutation = () => {
  const { refetch } = useAppContext();
  const { mutate, isLoading, isSuccess } = useMutation(
    ['team-page-query'],
    (eventId: Event['id']) =>
      cancelAppointment({
        eventId,
      }),
    {
      retry: false,
      onError: (err) => {
        console.log(err);
      },
      onSuccess: () => {
        refetch();
      },
    }
  );

  return { mutate, isLoading, isSuccess };
};

export const AppointmentCard = (props: AppointmentType) => {
  const { city, event, master, salon, payment_type } = props;
  const { t } = useTranslation('common');
  const {
    mutate: cancelAppointment,
    isSuccess,
    isLoading,
  } = useCancelAppointmentMutation();
  const { push } = usePushWithState();

  const date = useMemo(
    () => formatDateWithDayOfTheWeek(event.date_time, i18next.language),
    [event.date_time]
  );
  const formattedTime = useMemo(
    () => formatDateToHM(event.date_time),
    [event.date_time]
  );
  const formattedDate = date.slice(0, 1).toUpperCase() + date.slice(1);

  const markOnMap = useMemo(
    () =>
      salon.gps
        .split(',')
        .map((i) => Number(i))
        .filter((i) => !Number.isNaN(i)),
    [salon.gps]
  );

  const paymentRequired = event.payment_type === 4 && event.paid === 0;

  const handleShowOnMapClick = () => {
    if (!markOnMap) return;
    push('/maps', { props, lat: markOnMap[0], lng: markOnMap[1] });
  };

  const handleCancelAppointmentClick = () => cancelAppointment(event.id);

  return (
    <div className={styles.card}>
      <Button
        icon={
          <CalendarIcon className={styles.buttonIcon} width={23} height={23} />
        }
        size='small'
      >
        {formattedDate}
      </Button>
      <Button
        icon={
          <ClockIcon className={styles.buttonIcon} width={23} height={23} />
        }
        size='small'
      >
        {formattedTime}
      </Button>
      <Button
        icon={
          <HumanIcon className={styles.buttonIcon} width={23} height={23} />
        }
        size='small'
      >
        {t('master.master')} {master.name} {master.surname}
      </Button>
      <Button
        icon={
          <LocationIcon className={styles.buttonIcon} width={23} height={23} />
        }
        size='small'
      >
        {city.name}, {salon.name}
      </Button>
      <Button
        icon={<PaymentIconView isActive paymentType={payment_type || null} />}
        size='small'
      >
        {payment_type?.name}
      </Button>
      {paymentRequired && (
        <Button
          // icon={<PaymentIconView paymentType={paymentMethod || null} />}
          size='small'
        >
          <span className={styles.payWithing}>
            {t('appointments.payWithing')}
          </span>
        </Button>
      )}

      <TotalPrice
        price={`${event.price} ${getCurrencySign(event.currency)}`}
        paymentRequired={paymentRequired}
        paymentUrl={event.payment_url}
      />

      {markOnMap && (
        <ShowOnMap
          text={t('appointments.showOnMap')}
          onClick={handleShowOnMapClick}
        />
      )}

      <div className={styles.mainButtonWrapper}>
        <MainButton
          onClick={handleCancelAppointmentClick}
          text={t('appointments.cancelAppointment')}
          isLoading={isLoading || isSuccess}
        />
      </div>
    </div>
  );
};

import { AppointmentType } from '../../types';
import { AppointmentCard } from '../appointment-card';

import styles from './appointment-card-list.module.scss';

export const AppointmentCardList = ({ data }: { data: AppointmentType[] }) => {
  return (
    <div className={styles.cardList}>
      {data.map((i) => (
        <AppointmentCard {...i} key={i.event.id} />
      ))}
    </div>
  );
};

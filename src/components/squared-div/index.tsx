import styles from './squared-div.module.scss';

export const SquareDiv = ({
  children,
  maxWidth,
  styles: s = {},
}: {
  children: React.ReactNode;
  maxWidth?: string | number;
  styles?: React.CSSProperties;
}) => {
  return (
    <div
      className={styles.wrapper}
      style={{
        maxWidth,
        ...s,
      }}
    >
      <div />
      <div>{children}</div>
    </div>
  );
};
